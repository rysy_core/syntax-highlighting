# Syntax highlighting

Visual Studio code syntax highlighting for RISC-V assembler.
Names was choosed to get nice colors rather than to be accurate.

With [solarized light color theme](https://github.com/microsoft/vscode/blob/master/extensions/theme-solarized-light/themes/solarized-light-color-theme.json):

![Colors](colors.png)

## Install

To compile:

```
vsce package --baseImagesUrl https://gitlab.com/rysy_core/syntax-highlighting/raw/master/
```

In Extension Marketplace select ... and then Install from VSIX...

## What's in the folder

* This folder contains all of the files necessary for your extension.
* `package.json` - this is the manifest file in which you declare your language support and define the location of the grammar file that has been copied into your extension.
* `syntaxes/risc-v.tmLanguage.json` - this is the Text mate grammar file that is used for tokenization.
* `language-configuration.json` - this is the language configuration, defining the tokens that are used for comments and brackets.

## Resources

 * https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide
 * https://github.com/zhuanhao-wu/vscode-riscv-support
 * https://github.com/ReinForce-II/vscode-riscv/
 * https://github.com/tvi/Sublime-ARM-Assembly/blob/master/Syntaxes/ARM%20Assembly.tmLanguage
 * https://github.com/aumuell/ARM-assembly.tmbundle/blob/master/Syntaxes/ARM%20Assembly.tmLanguage
 * https://github.com/kdarkhan/vscode-mips-support
